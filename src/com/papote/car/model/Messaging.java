package com.papote.car.model;

public class Messaging {

    public Trajet trajet;
    public User[] users;
    public String[] Messages;

    public Trajet getTrajet() {
        return trajet;
    }

    public void setTrajet(Trajet trajet) {
        this.trajet = trajet;
    }

    public User[] getUsers() {
        return users;
    }

    public void setUsers(User[] users) {
        this.users = users;
    }

    public String[] getMessages() {
        return Messages;
    }

    public void setMessages(String[] messages) {
        Messages = messages;
    }
}

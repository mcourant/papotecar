package com.papote.car.model;

public class Voyage {

    private Step start_step;
    private Step end_step;
    private User voyageur;

    public Step getStart_step() {
        return start_step;
    }

    public void setStart_step(Step start_step) {
        this.start_step = start_step;
    }

    public Step getEnd_step() {
        return end_step;
    }

    public void setEnd_step(Step end_step) {
        this.end_step = end_step;
    }

    public User getVoyageur() {
        return voyageur;
    }

    public void setVoyageur(User voyageur) {
        this.voyageur = voyageur;
    }
}

package com.papote.car.model;

import java.util.ArrayList;
import java.util.List;

public class Trajet{

    private String start_date;
    private String start_hours;
    private List<Step> step = new ArrayList<>();
    private List<Voyage> voyages = new ArrayList<>();
    private Car car;

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getStart_hours() {
        return start_hours;
    }

    public void setStart_hours(String start_hours) {
        this.start_hours = start_hours;
    }

    public List<Step> getStep() {
        return step;
    }

    public void setStep(Step step) {
        this.step.add(step);
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public void setVoyage(Voyage voyage) {
        this.voyages.add(voyage);
    }

    public List<Voyage> getVoyages() {
        return voyages;
    }

    public boolean estCompatible(Voyage voyage){
        boolean result_start = false;
        boolean result_end = false;
        for (Step s: step){
            if(s.getName().compareTo(voyage.getStart_step().getName()) == 0){
                result_start = true;
            }
            if(s.getName().compareTo(voyage.getEnd_step().getName()) == 0){
                result_end = true;
            }
        }
        if(result_end && result_start){
            return true;
        }else{
            return false;
        }
    }

    public boolean estRealisable(Voyage voyage){
        int placelibre = car.getPlace_libre();
        boolean result = true;
        for (Step step : this.step){
            for (Voyage v : this.voyages){

                if(step.getName().compareTo(v.getEnd_step().getName()) == 0){
                    placelibre++;
                }

                if(step.getName().compareTo(v.getStart_step().getName()) == 0){
                    placelibre--;
                }
                if(placelibre == -1){
                    return false;
                }
            }
        }
        return result;
    }

    public void toStringOutput() {
        System.out.println("Le trajet ce compose : " + "\n" +
                "Voiture : Driver = "+ car.getDriver().getFirstname()+ " | Nbr de place : "+car.getPlaces());

        for (Step step : this.step){
            System.out.println("Etape "+step.getNumberOfStep()+ " | Nom : "+step.getName());
            for (Voyage voyage : this.voyages){

                if(step.getName().compareTo(voyage.getEnd_step().getName()) == 0){
                    System.out.println( voyage.getVoyageur().getFirstname() + " descend");
                    car.setPlace_libre(car.getPlace_libre()+1);
                    System.out.println("Place libre : "+car.getPlace_libre());
                }

                if(step.getName().compareTo(voyage.getStart_step().getName()) == 0){
                    System.out.println( voyage.getVoyageur().getFirstname() + " monte");
                    car.setPlace_libre(car.getPlace_libre()-1);
                    System.out.println("Place libre : "+car.getPlace_libre());
                }
            }
            System.out.println("-------------");
        }
    }
}
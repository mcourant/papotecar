package com.papote.car.model;

public class Step{

    private String name;
    private String numberOfStep;
    private String name_city;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumberOfStep() {
        return numberOfStep;
    }

    public void setNumberOfStep(String numberOfStep) {
        this.numberOfStep = numberOfStep;
    }

    public String getName_city() {
        return name_city;
    }

    public void setName_city(String name_city) {
        this.name_city = name_city;
    }

    @Override
    public String toString() {
        return  "\n" +" Ma step : " +  "\n" +
                "name = '" + name +  "\n" ;
    }
}
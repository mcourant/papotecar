package com.papote.car.model;

import java.util.ArrayList;
import java.util.List;

public class Car{

    private User driver;
    private int places;
    private int place_libre;

    public int getPlace_libre() {
        return place_libre;
    }

    public void setPlace_libre(int place_libre) {
        this.place_libre = place_libre;
    }

    public User getDriver() {
        return driver;
    }

    public void setDriver(User driver) {
        this.driver = driver;
    }

//    public List<User> getPassager() {
//        return passagers;
//    }
//
//    public void setPassager(User passager) {
//        if(this.passagers.size() < this.getPlaces()){
//            System.out.println(passager.getFirstname()+" a été charger");
//            place_libre--;
//            System.out.println("Place libre "+place_libre+" / "+places);
//            this.passagers.add(passager);
//        }else{
//            System.out.println("Pas assez de place pour " + passager.getFirstname());
//        }
//    }
//
//    public void removePassager(User passager){
//        if(this.passagers.contains(passager)){
//            this.passagers.remove(passager);
//            System.out.println(passager.getFirstname() + " descend");
//            place_libre++;
//            System.out.println("Place libre "+place_libre+" / "+places);
//        }
//    }

    public int getPlaces() {
        return places;
    }

    public void setPlaces(int places) {
        this.places = places;
        this.place_libre = places;
    }

    @Override
    public String toString() {
        return  "\n" +
                "driver = " + driver.getFirstname() +  "\n" +
                "places = " + places +  "\n" ;
    }

}
package com.papote.car.model;

public class User{

    private String firstname;
    private String lastname;
    private Boolean isCoducteur;
    private Boolean isPassager;
    private String login;
    private String password;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Boolean getCoducteur() {
        return isCoducteur;
    }

    public void setCoducteur(Boolean coducteur) {
        isCoducteur = coducteur;
    }

    public Boolean getPassager() {
        return isPassager;
    }

    public void setPassager(Boolean passager) {
        isPassager = passager;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public Boolean checkLogin(String login, String password){
        Boolean checkup = false;
        if(this.login.equals(login) && this.password.equals(password)){
            checkup = true;
        }
        return checkup;
    }

    @Override
    public String toString() {
        return  "\n" +
                "firstname = '" + firstname +  "\n" +
                "isCoducteur = " + isCoducteur + "\n" +
                "isPassager = " + isPassager + "\n" ;
    }


}
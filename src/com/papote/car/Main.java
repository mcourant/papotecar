package com.papote.car;


import com.papote.car.model.*;
import com.papote.car.util.InitApp;
import com.papote.car.util.Menu;

import java.util.List;

public class Main {


    public static void main(String[] args) {

        Car car = new Car();
        User conducteur = new User();
        User passager1 = new User();
        User passager2 = new User();
        User passager3 = new User();
        User passager4 = new User();
        Trajet trajet = new Trajet();

        conducteur.setFirstname("Maxime");
        conducteur.setCoducteur(true);

        passager1.setFirstname("Paul");
        passager1.setPassager(true);

        passager2.setFirstname("Pierre");
        passager2.setPassager(true);

        passager3.setFirstname("Jacques");
        passager3.setPassager(true);

        passager4.setFirstname("Louis");
        passager4.setPassager(true);

        car.setDriver(conducteur);
        car.setPlaces(2);

        trajet.setCar(car);

        Step paris = new Step();
        paris.setName("Paris");
        paris.setNumberOfStep("1");
        paris.setName_city("Paris");

        Step poitiers = new Step();
        poitiers.setName("Poitiers");
        poitiers.setNumberOfStep("2");
        poitiers.setName_city("Poitiers");

        Step niort = new Step();
        niort.setName("Niort");
        niort.setNumberOfStep("3");
        niort.setName_city("Niort");

        Step bordeaux = new Step();
        bordeaux.setName("Bordeaux");
        bordeaux.setNumberOfStep("4");
        bordeaux.setName_city("Bordeaux");

        Step marseille = new Step();
        marseille.setName("Marseille");
        marseille.setNumberOfStep("5");
        marseille.setName_city("Marseille");

        trajet.setStep(paris);
        trajet.setStep(poitiers);
        trajet.setStep(niort);
        trajet.setStep(bordeaux);

        Voyage voyagepaul = new Voyage();
        voyagepaul.setStart_step(paris);
        voyagepaul.setEnd_step(bordeaux);
        voyagepaul.setVoyageur(passager1);
        if(trajet.estRealisable(voyagepaul) && trajet.estCompatible(voyagepaul)){
            trajet.setVoyage(voyagepaul);
        }else{
            System.out.println("Impossible d'ajouter "+passager1.getFirstname());
        }

        Voyage voyagelouis = new Voyage();
        voyagelouis.setStart_step(paris);
        voyagelouis.setEnd_step(poitiers);
        voyagelouis.setVoyageur(passager4);
        if(trajet.estRealisable(voyagelouis) && trajet.estCompatible(voyagelouis)){
            trajet.setVoyage(voyagelouis);
        }else{
            System.out.println("Impossible d'ajouter "+passager4.getFirstname());
        }

        Voyage voyagepierre = new Voyage();
        voyagepierre.setStart_step(poitiers);
        voyagepierre.setEnd_step(niort);
        voyagepierre.setVoyageur(passager2);
        if(trajet.estRealisable(voyagepierre) && trajet.estCompatible(voyagepierre)){
            trajet.setVoyage(voyagepierre);
        }else{
            System.out.println("Impossible d'ajouter "+passager2.getFirstname());
        }

        Voyage voyagejacques = new Voyage();
        voyagejacques.setStart_step(poitiers);
        voyagejacques.setEnd_step(bordeaux);
        voyagejacques.setVoyageur(passager3);
        if(trajet.estRealisable(voyagejacques) && trajet.estCompatible(voyagejacques)){
            trajet.setVoyage(voyagejacques);
        }else{
            System.out.println("Impossible d'ajouter "+passager3.getFirstname());
        }

        trajet.toStringOutput();

    }

}

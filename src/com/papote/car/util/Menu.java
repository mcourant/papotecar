package com.papote.car.util;

import java.util.Scanner;

public class Menu {

    public void showMenu(){
        System.out.println("Choisir un menu !");

        System.out.println("1 - Dashboard");
        System.out.println("2 - Gérer mes trajets");
        System.out.println("3 - Trouver un trajet");
        System.out.println("4 - Mes messages");

        Scanner sc = new Scanner(System.in);
        System.out.println("Indiquez le numéro de votre choix : ");
        Integer choice = 0;
        try {
            choice = Integer.parseInt(sc.nextLine());
        } catch(NumberFormatException e) {
            System.out.println("Ce n'est pas un numéro");
            this.showMenu();
        }

        switch (choice){
            case 1:
                break;
            case 2:
                this.showMenuTrajet();
                break;
            case 3:
                break;
            case 4:
                break;
            default:
                break;
        }

    }

    private void showMenuTrajet(){
        System.out.println("Trajet admin !");

        System.out.println("1 - Ajouter un trajet");
        System.out.println("2 - Modifier un trajet");
        System.out.println("3 - Supprimer un trajet");

        Integer choice = 0;

        Scanner sc = new Scanner(System.in);
        try {
            choice = Integer.parseInt(sc.nextLine());
        } catch(NumberFormatException e) {
            System.out.println("Ce n'est pas un numéro");
            this.showMenuTrajet();
        }

        switch(choice){
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            default:
                break;
        }

    }

}
